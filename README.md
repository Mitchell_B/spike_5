# Spike Report

## SPIKE 5

### Introduction

The purpose of this spike is to create a Main Menu and Pause Menu for the game.

### Goals

1. A Heads-Up Display which displays some values of the Pawn which we are possessing. You can do this in one of several ways:
    - [Preferred] In your custom PlayerController, create your Widget and attach it
    - [Rudimentary] In the Level Blueprint, create/attach the Widget to Player Controller 0
    - [Depreciated] Add a custom HUD class to your custom GameMode

1. A Main Menu, in a separate scene, which has 3 buttons:
    - Start a New Game
    - Show some Info (Gameplay controls? Credits?)
    - Or Quit the game

1. A Pause Menu, during which:
    - The game pauses, and the mouse is shown (if hidden)
    - The player can Return to the Game (re-hides the mouse)
    - The player can Go to the Menu (change scene)

### Personnel

* Primary - Mitchell
* Secondary - N/A

### Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

* [UMG UI Designer Quick Start Guide](https://docs.unrealengine.com/latest/INT/Engine/UMG/QuickStart/index.html)

### Tasks Undertaken

Show only the key tasks that will help the developer/reader understand what is required.

1. Followed UMG UI Designer Quick Start Guide.
    1. Create a new level and add Main Menu widget to the level blueprint.
        - Link play button to main game level.
    1. Create an input function for pause to be activated on press.
        - Done in action mapping.
    1. Inside the Player Controller, make it possible to pause the game using the pause action.
        - On press, create the pause widget.
        - Set mouse cursor to show the mouse cursor.
        - Set input mode to UI only.
        - Add to viewport and set the game to paused.

### What we found out

- How widgets function.
- How menus can share a widget using visibility.