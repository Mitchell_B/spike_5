// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameStateBase.h"
#include "GS_Spike_4.generated.h"

/**
 * 
 */
UCLASS()
class SPIKE_4_API AGS_Spike_4 : public AGameStateBase
{
	GENERATED_BODY()

public:
	// enables contructor
	AGS_Spike_4();

	void BeginPlay() override;

	void Tick(float DeltaTime) override;

	//Have a start time of 30seconds
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float StartTime = 30.f;

	// The time that counts down from 30seconds
	UPROPERTY(BlueprintReadonly, VisibleAnywhere)
		float CurrentTime = 30.f;


};
