// Fill out your copyright notice in the Description page of Project Settings.

#include "Spike_4.h"
#include "GS_Spike_4.h"
#include "Spike_4GameModeBase.h"

// enables constructor to use tick functions
AGS_Spike_4::AGS_Spike_4()
{
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;
}

// on begin play.
void AGS_Spike_4::BeginPlay()
{
	Super::BeginPlay();

	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(2, 5.f, FColor::Red, FString::Printf(TEXT("BegunPlay")));
	}
}

// When game starts count down from 30 -> 0
// When it hits 0 call game mode
void AGS_Spike_4::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CurrentTime -= DeltaTime;
	// show player the count down state.
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Red, FString::Printf(TEXT("Time: %f"), CurrentTime));
	}

	if (CurrentTime <= 0.f)

	{
		auto GM = Cast<ASpike_4GameModeBase>(GetWorld()->GetAuthGameMode());
		// calls the game mode for the game over logic
		GM->GameOver();
	}
}

