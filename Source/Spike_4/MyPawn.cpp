// Fill out your copyright notice in the Description page of Project Settings.

#include "Spike_4.h"
#include "MyPawn.h"


// Sets default values
AMyPawn::AMyPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Set this pawn to be controlled by the lowest-numbered player
	AutoPossessPlayer = EAutoReceiveInput::Player0;

	// Create a dummy root component we can attach things to.
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	// Create a camera and a visible object
	UCameraComponent* OurCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("OurCamera"));
	OurVisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("OurVisibleComponent"));
	// Attach our camera and visible object to our root component. Offset and rotate the camera.
	OurCamera->SetupAttachment(RootComponent);
	OurCamera->SetRelativeLocation(FVector(-250.0f, 0.0f, 250.0f));
	OurCamera->SetRelativeRotation(FRotator(-45.0f, 0.0f, 0.0f));
	OurVisibleComponent->SetupAttachment(RootComponent);

	bUseControllerRotationYaw = true;
}

// Called when the game starts or when spawned
void AMyPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Handle movement based on our "MoveX" and "MoveY" axis
	UpdateVelocity();

	if (!CurrentVelocity.IsZero())
	{
		FVector NewLocation = GetActorLocation() + (CurrentVelocity * DeltaTime);
		SetActorLocation(NewLocation);
	}
}

void AMyPawn::Forward(float AxisValue)
{
	auto NewVelocityX = FMath::Clamp(AxisValue, -1.0f, 1.0f);

	NewVelocityX *= 100;

	if (IsRunning)
	{
		NewVelocityX *= 5;
	}

	DesiredForwardSpeed = NewVelocityX;
}

void AMyPawn::Right(float AxisValue)
{
	auto NewVelocityY = FMath::Clamp(AxisValue, -1.0f, 1.0f);

	NewVelocityY *= 100;

	if (IsRunning)
	{
		NewVelocityY *= 5;
	}

	DesiredRightSpeed = NewVelocityY;
}

void AMyPawn::Sprint(bool NowRunning)
{
	this->IsRunning = NowRunning;
}

void AMyPawn::UpdateVelocity()
{
	FVector NewVelocity{ DesiredForwardSpeed, DesiredRightSpeed, 0 };

	//NewVelocity = NewVelocity.GetClampedToMaxSize(1.0f);

	CurrentVelocity = GetControlRotation().RotateVector(NewVelocity);
}